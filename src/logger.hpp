/*
 *  SatNOGS-COMMS CLI control software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <memory>
#include <satnogs-comms-proto/satnogs-comms.pb.h>
#include <spdlog/spdlog.h>
#include <string>

class logger
{

public:
  using sptr = std::shared_ptr<logger>;

  enum class target
  {
    CONSOLE = 0,
    FILE    = 1
  };

  static sptr
  make_shared(const std::string &send_log_fname,
              const std::string &recv_log_fname);

  void
  send(std::initializer_list<target> list, const satnogs_comms &msg);

  void
  send(const satnogs_comms &msg);

  void
  recv(std::initializer_list<target> list, const satnogs_comms &msg);

  void
  recv(const satnogs_comms &msg);

  void
  console(const satnogs_comms &msg);

  template <typename... Args>
  void
  console(spdlog::level::level_enum level, spdlog::format_string_t<Args...> fmt,
          Args &&...args)
  {
    m_console_log->log(level, fmt, args...);
  }

protected:
  logger(const std::string &send_log_fname, const std::string &recv_log_fname);

private:
  std::shared_ptr<spdlog::logger> m_send_log;
  std::shared_ptr<spdlog::logger> m_recv_log;
  std::shared_ptr<spdlog::logger> m_console_log;
};
