/*
 *  SatNOGS-COMMS CLI control software
 *
 *  Copyright (C) 2023-2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */
#include "argagg.hpp"
#include "can_transport.hpp"
#include "gr_zmq_transport.hpp"
#include "logger.hpp"
#include <chrono>
#include <cli/boostasioscheduler.h>
#include <cli/cli.h>
#include <cli/clilocalsession.h>
#include <cli/filehistorystorage.h>
#include <iostream>
#include <random>
#include <yaml-cpp/yaml.h>

using namespace std::chrono_literals;
using rnd_bytes_engine =
    std::independent_bits_engine<std::default_random_engine, CHAR_BIT,
                                 unsigned char>;

bool running = true;

void
signal_handler(int s)
{
  running = false;
}

void
handle_exception(std::exception &e, logger::sptr logger)
{
  logger->console(spdlog::level::err, "Exception: {}", e.what());
}

void
rx_thread(transport::sptr tr)
{
  while (running) {
    satnogs_comms msg;
    try {
      tr->recv(msg);
    } catch (...) {
    }
  }
}

void
setup_signal_handler()
{
  struct sigaction shandler;
  shandler.sa_handler = signal_handler;
  sigemptyset(&shandler.sa_mask);
  shandler.sa_flags = 0;
  sigaction(SIGINT, &shandler, NULL);
}

argagg::parser
setup_arg_parser()
{
  return argagg::parser{
      {{"help", {"-h", "--help"}, "shows this help message", 0},
       {"config", {"-c", "--config"}, "File path of the configuration file", 1},
       {"log_recv",
        {"--log-recv"},
        "filename to store the log of the received messages. Explicit "
        "specifying this option has priority over the one set in the "
        "configuration file",
        1},
       {"log_send",
        {"--log-send"},
        "filename to store the log of the sent messages. Explicit specifying "
        "this option has priority over the one set in the configuration file",
        1}}};
}

argagg::parser_results
parse_args(argagg::parser &argparser, int argc, char **argv)
{
  try {
    return argparser.parse(argc, argv);
  } catch (const std::exception &e) {
    std::cout << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }
}

static std::string
get_log_recv(argagg::parser_results &args, YAML::Node &config)
{
  if (!args.has_option("log_recv") && !config["log-recv"]) {
    return "/dev/null";
  } else {
    return args.has_option("log_recv") ? args["log_recv"].as<std::string>()
                                       : config["log-recv"].as<std::string>();
  }
}

static std::string
get_log_send(argagg::parser_results &args, YAML::Node &config)
{
  if (!args.has_option("log_send") && !config["log-send"]) {
    return "/dev/null";
  } else {
    return args.has_option("log_send") ? args["log_send"].as<std::string>()
                                       : config["log-send"].as<std::string>();
  }
}

static transport::sptr
setup_transport(argagg::parser_results &args, YAML::Node &config,
                logger::sptr logger)
{
  if (config["transport"]["gr-zmq"]) {
    if (config["transport"]["gr-zmq"]["frame_size"]) {
      return gr_zmq_transport::make_shared(
          config["transport"]["gr-zmq"]["PUB_URI"].as<std::string>(),
          config["transport"]["gr-zmq"]["SUB_URI"].as<std::string>(),
          config["transport"]["gr-zmq"]["frame_size"].as<size_t>(), logger);
    } else {
      return gr_zmq_transport::make_shared(
          config["transport"]["gr-zmq"]["PUB_URI"].as<std::string>(),
          config["transport"]["gr-zmq"]["SUB_URI"].as<std::string>(), logger);
    }
  }

  if (config["transport"]["canbus"]) {
    return can_transport::make_shared(
        config["transport"]["canbus"]["iface"].as<std::string>(),
        config["transport"]["canbus"]["tx-id"].as<uint32_t>(),
        config["transport"]["canbus"]["rx-id"].as<uint32_t>(),
        config["transport"]["canbus"]["remote-tx-id"].as<uint32_t>(),
        config["transport"]["canbus"]["remote-rx-id"].as<uint32_t>(), logger);
  }
  throw std::runtime_error("Invalid transport type");
}

void
configure_root_menu(cli::Menu *root_menu, transport::sptr tr,
                    logger::sptr logger)
{
  root_menu->Insert(
      "ping", {"size"},
      [tr, logger](std::ostream &, size_t size) {
        if (size > tr->max_msg_len()) {
          logger->console(spdlog::level::warn,
                          "The maximum supported message is {}",
                          tr->max_msg_len());
          return;
        }
        satnogs_comms    msg;
        rnd_bytes_engine rng;
        auto x = msg.mutable_tlc()->mutable_ping()->mutable_payload();
        for (size_t i = 0; i < size; i++) {
          auto b = rng();
          x->append(1, b);
        }
        tr->send(msg);
      },
      "Basic ping command with arbitrary size");
}

void
configure_tlm_menu(cli::Menu *tlm_menu, transport::sptr tr)
{
  tlm_menu->Insert(
      "basic", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        msg.mutable_tlc()->mutable_tlm_req()->set_type(telemetry_type::BASIC);
        tr->send(msg);
      },
      "Request basic telemetry");
  tlm_menu->Insert(
      "health", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        msg.mutable_tlc()->mutable_tlm_req()->set_type(telemetry_type::HEALTH);
        tr->send(msg);
      },
      "Request health telemetry");
  tlm_menu->Insert(
      "config", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        msg.mutable_tlc()->mutable_tlm_req()->set_type(telemetry_type::CONFIG);
        tr->send(msg);
      },
      "Request configuration telemetry");
  tlm_menu->Insert(
      "fpga", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        msg.mutable_tlc()->mutable_tlm_req()->set_type(telemetry_type::FPGA);
        tr->send(msg);
      },
      "Request FPGA telemetry");
  tlm_menu->Insert(
      "radio", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        msg.mutable_tlc()->mutable_tlm_req()->set_type(telemetry_type::RADIO);
        tr->send(msg);
      },
      "Request RADIO telemetry");
  tlm_menu->Insert(
      "power", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        auto          x = msg.mutable_tlc()->mutable_pwr();
        tr->send(msg);
      },
      "Request power telemetry");
  tlm_menu->Insert(
      "enable-periodic-basic", {"period_ms"},
      [tr](std::ostream &, size_t period_ms) {
        satnogs_comms msg;
        auto          x = msg.mutable_tlc()->mutable_tlm_periodic();
        x->set_enable(true);
        x->set_period_ms(period_ms);
        x->set_type(telemetry_type::BASIC);
        tr->send(msg);
      },
      "Enable periodic basic telemetry");
  tlm_menu->Insert(
      "disable-periodic", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        auto          x = msg.mutable_tlc()->mutable_tlm_periodic();
        x->set_enable(false);
        tr->send(msg);
      },
      "Disable periodic basic telemetry");
}

void
configure_test_menu(cli::Menu *test_menu, transport::sptr tr)
{
  test_menu->Insert(
      "uhf-tx-simple", {"nframes", "delayus"},
      [tr](std::ostream &, size_t n, size_t delay = 1000) {
        satnogs_comms msg;
        auto          x = msg.mutable_tlc()->mutable_test()->mutable_start();
        x->set_id(test_id::UHF_TX_SIMPLE);
        x->set_param0(n);
        x->set_param1(delay);
        tr->send(msg);
      },
      "Execute nframes with a simple TX configuration at the UHF");
  test_menu->Insert(
      "uhf-ota", {"nframes", "delayus"},
      [tr](std::ostream &, size_t n, size_t delay = 1000) {
        satnogs_comms msg;
        auto          x = msg.mutable_tlc()->mutable_test()->mutable_start();
        x->set_id(test_id::UHF_TX_SIMPLE);
        x->set_param0(n);
        x->set_param1(delay);
        tr->send(msg);
      },
      "Execute OTA at the UHF");
  test_menu->Insert(
      "sband-tx-simple", {"nframes", "delayus"},
      [tr](std::ostream &, size_t n, size_t delay = 1000) {
        satnogs_comms msg;
        auto          x = msg.mutable_tlc()->mutable_test()->mutable_start();
        x->set_id(test_id::SBAND_TX_SIMPLE);
        x->set_param0(n);
        x->set_param1(delay);
        tr->send(msg);
      },
      "Execute nframes with a simple TX configuration at the S-Band");
  test_menu->Insert(
      "stop", {},
      [tr](std::ostream &) {
        satnogs_comms msg;
        auto          x = msg.mutable_tlc()->mutable_test()->mutable_stop();
        tr->send(msg);
      },
      "Stop any pending test");
}

void
handle_exception(const std::string &cmd, const std::exception &e,
                 logger::sptr logger)
{
  logger->console(spdlog::level::err,
                  "Exception caught in cli handler: {} handling command: {}.",
                  e.what(), cmd);
}

void
setup_exit_action(cli::CliLocalTerminalSession &local_session,
                  cli::BoostAsioScheduler &scheduler, logger::sptr logger)
{
  local_session.ExitAction([&scheduler, logger](auto &) {
    logger->console(spdlog::level::info, "Closing controller...");
    scheduler.Stop();
    running = false;
  });
}

void
setup_cli(std::unique_ptr<cli::Menu> root_menu, logger::sptr logger)
{
  cli::Cli cli(std::move(root_menu),
               std::make_unique<cli::FileHistoryStorage>(".satnogs-comms"));
  cli.StdExceptionHandler(
      [logger](std::ostream &, const std::string &cmd,
               const std::exception &e) { handle_exception(cmd, e, logger); });
  cli::BoostAsioScheduler      scheduler;
  std::size_t                  history_size = 200;
  cli::CliLocalTerminalSession local_session(cli, scheduler, std::cout,
                                             history_size);
  setup_exit_action(local_session, scheduler, logger);
  scheduler.Run();
  return;
}

int
main(int argc, char **argv)
{
  setup_signal_handler();

  auto argparser = setup_arg_parser();
  auto args      = parse_args(argparser, argc, argv);
  if (args["help"]) {
    std::cout << argv[0] << " [OPTIONS] " << std::endl;
    std::cout << argparser << std::endl;
    return EXIT_SUCCESS;
  }

  YAML::Node config;
  config   = YAML::LoadFile(args["config"].as<std::string>());
  auto log = logger::make_shared(get_log_send(args, config),
                                 get_log_recv(args, config));
  auto tr  = setup_transport(args, config, log);

  std::thread rx(rx_thread, tr);
  try {
    cli::CmdHandler colored_cmd;
    auto            root_menu = std::make_unique<cli::Menu>("satnogs-comms");
    configure_root_menu(root_menu.get(), tr, log);
    auto tlm_menu = std::make_unique<cli::Menu>("telemetry");
    configure_tlm_menu(tlm_menu.get(), tr);
    root_menu->Insert(std::move(tlm_menu));
    auto test_menu = std::make_unique<cli::Menu>("tests");
    configure_test_menu(test_menu.get(), tr);
    root_menu->Insert(std::move(test_menu));
    setup_cli(std::move(root_menu), log);

  } catch (std::exception &e) {
    running = false;
  }
  tr->shutdown();
  rx.join();
  return EXIT_SUCCESS;
}